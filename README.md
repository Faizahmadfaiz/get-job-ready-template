From [Get Job ready with 1 FCC cert, 3 projects, 2 courses, and 10 books](https://forum.freecodecamp.com/t/computer-guide-get-job-ready-with-1-fcc-cert-3-projects-2-courses-and-10-books/64027/1)

# Ok, I get it, a ton of campers want the fast track to job ready.


----------

Do you want to be job ready (for a junior role) without finishing all three certifications? Want to short-circuit the process and get this done quick without getting laughed out of interviews?

Are you thinking "I'll just finish the frontend certification" and start applying?

**You're looking at the wrong certification** to get prepared and I'll tell you why. The frontend certification does NOT teach you any of the real world skills that go along with programming that you'll need on the job. It doesn't teach you how to work in an editor on your own machine. It doesn't teach you how to run a build process. It doesn't teach you how to share your code in a git repository. And, it doesn't teach you how to deploy your code to a real server on the net.

**Have no fear** - there IS a FCC certification that will get you much, much closer to your goal. Combine it with just a few other resources and you can get there. The backend certification is the one you want to be aiming for. 

#### Finish this list - and you'll walk into interviews with a lot more confidence, and a much better chance of landing that job.



## Read these books, complete these two courses, this certification, and these three projects


----------

* All 6 books in the [YDKJS Series](https://github.com/getify/You-Dont-Know-JS)
* Course - [CS50](https://courses.edx.org/courses/course-v1%3AHarvardX%2BCS50%2BX/)  on edX
* Book - [JavaScript Allongé, the "Six" Edition](https://leanpub.com/javascriptallongesix)
* Course - [Introduction to MongoDB using the MEAN Stack](https://www.edx.org/course/introduction-mongodb-using-mean-stack-mongodbx-m101x-0) on edX
* The FCC Backend Certification - all projects, ignore the tutorials and just read the official docs if you want.
* Book - [DOM Enlightenment](http://domenlightenment.com/)
* Project - Portfolio from the frontend section of FCC
* Book - [JavaScript Design Patterns](https://addyosmani.com/resources/essentialjsdesignpatterns/book/)
* Project - Clone the Netflix interface using React, Angular, or Vue - pulling data from [the Movie DB API](https://www.themoviedb.org/documentation/api) or an API in a backend you create yourself.
* Book - [Open Data Structures](http://www.aupress.ca/books/120226/ebook/99Z_Morin_2013-Open_Data_Structures.pdf)
* Project - Clone [this Admin template](http://rubix410.sketchpixy.com/ltr/dashboard) using React, Angular, or Vue

**_Special note: No, you're not done until you finish every step, in order._**



----------

### Notes (keep all this in mind as you work down the list):

----------

### Development Enviroment
* Pick your favorite editor, write all your code on your own machine in that editor
* Save all your code to GitHub
* Publish all your projects to the web  - using surge.sh, github pages, heroku etc
* Avoid codepen.
* Avoid cloud 9 or any other "just code it on the web so you don't have to learn how to code on your own machine" site.
* Whenever you are doing a "Clone this" project - use only  assets you create yourself or that have copyrights which would allow you to use them.
* Develop the habit of giving attribution for everything you use that was the product of someone else's effort.

### Power up your learning - make every minute count towards making you job ready
* When you're working through a book, type out the example code and do all the exercises.
* When you're working through a course, do all the assignments.
* When you're building a Project, include a built process (like [gulp](http://gulpjs.com/)), include at least 5 automated tests ([Karma](https://karma-runner.github.io/1.0/index.html), [mocha](https://mochajs.org/), sinon, chai, jasmine, tape, whatever works for you), include a README.md file that clearly explains the project, and practice using [meaningful 
commit messages](http://chris.beams.io/posts/git-commit/) 
* Use each project to demonstrate that you can deliver an optimized product. Implement tests (as I mentioned above), use your build process to minimize assets, use a linter (like [ESLint](http://eslint.org/)) on your JavaScript and have some method to your madness when it comes to your css naming conventions (like [BEM](http://getbem.com/introduction/))

Every resource on this list is freely available on the net. And, it was meant, by the author, to be free. There's no "you can find a pirated copy if you look hard enough" nonsense here. No resource listed suffers for this. There are other comparable resources you could pay for, none of the paid versions would give you some awesome advantage over those listed.

This is the "direct route" to your goal. I didn't promise it would be easy though. You'll need to put your big programmer pants on and barrel through even when it gets tough. Keep the goal in mind. Don't go wandering off looking for 12 different extra resources each step of the path, suck it up and stick to the list and keep at each step until you finish it. This is not the "easy path", this is the "work your ass off, keep at it, don't go wandering off to YouTube muttering 'oooh shiny'" path.

#### If you get stuck and need to look something up: 
* Use the official docs.
* Use MDN
* Train yourself to write the code, not copy paste it.
* Stack overflow should be a resource for ideas, not a crutch to help you avoid writing code yourself.
* [Shay Howe](http://learn.shayhowe.com/) has a good overview of HTML and CSS if you need more than you got in the CS50 and MEAN stack course to feel comfortable with the projects.
* If you're at a loss for how to figure out the design for a page you're building, reference [Google's Material Design documentation](https://material.google.com/). Try to become familar with it by the time you finish the list of resources in this post. It will save you from having shoddy looking pages.

#### If you get lost. If this is too hard.
* Buck up camper - no one said this was easy - it is WORTH IT though.
* Figure it out. Ask for someone to point you in the right direction.
* Don't Quit.
* Don't Panic!

#### If you don't even know where to start with GitHub and/or Surge.sh
#### Using GitHub
[thenewboston's Git Tutorials Playlist on YouTube](https://www.youtube.com/playlist?list=PL6gx4Cwl9DGAKWClAD_iKpNC0bGHxGhcx)
[The Git Tutorial on CodeSchool](https://try.github.io/)
[GitHub site](https://github.com/)
[Git Cheatsheet](https://education.github.com/git-cheat-sheet-education.pdf) - new link, the old one changed

#### Using surge.sh
[YouTube video by Hiếu Sensei walking through the entire process](https://www.youtube.com/watch?v=W10ckhQ1H7c)
[YouTube video by David Wells that goes into far more detail](https://www.youtube.com/watch?v=LZA8QVLOinE)
[Very brief article and video showing how to quickly deploy to surge.sh](http://toolsfortheweb.net/hosting/using-surge-for-free-static-site-hosting/)
[surge.sh site](https://surge.sh/)

#### if you're on fire and want more to boost your chances even higher or if you need a "productive break" from the main guide
* sign up for https://www.codingame.com - finish every training challenge up through "very hard", for the hard and very hard challenges, you'll likely need to have worked down the main list in this post to the data structures book
* learn [Sass](http://sass-lang.com/guide)
* Join one of the Chingu cohorts that @tropicalchancer puts together and actively participate on one or more group projects
* Project: Clone [this](https://blackrockdigital.github.io/startbootstrap-creative/)  landing page
* Project: Clone [this](https://blackrockdigital.github.io/startbootstrap-clean-blog/) blog template - note that there are multiple pages
* Project: Clone [this](https://creativemarket.com/ikonome/686585-Material-Resume-Blue/screenshots/#screenshot2) online resume template
* Project: Clone the front page of [this](https://urbanarmorgear.com/) website modifying it to highlight a different product or industry. Make special note of the secondary navigation bar at the top. Implementing that is the highlight of this project.
* Project: Do one of the FCC projects not listed in this guide.
* Goal: Contribute one pull request per month to an open source project of your choosing.

#### If you're thinking @P1xt, you're nuts, I just want to be a frontend developer, this is too much
* No it's not.
* This will actually do what the frontend cert alone wont. It will prepare you to do the job of a frontend web developer in today's market. Writing some HTML and CSS isn't all there is to the job. This will prepare you for the rest.
* Don't quit.
* Don't panic!


**Note**: Don't ask me how long this will take you. I do not know your aptitude, determination, or propensity for flitting off to check Facebook every 5 minutes, nor do I know how much time you have available to devote daily. Begin working through it and gauge for yourself the speed with which you progress. I promise you, if you just start from the top and work your way down, you won't accidentally learn something useless while you figure out your learning pace.